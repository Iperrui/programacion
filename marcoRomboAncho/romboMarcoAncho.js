var numAncho = 9;
var res = "";
var espacios = 0;
main(numAncho);
function pintarBlancos(espacios) {
    for (var I = 0; I < espacios; I++) {
        res += " ";
    }
}
function pintarFila(numCaracter, ancho) {
    var totalPintar = numCaracter;
    for (var I = 0; I < ancho + 1; I++) {
        if (totalPintar > 0) {
            res += "#";
            totalPintar -= 1;
        }
    }
}
function pintarMarco(ancho) {
    res += "|";
}
function pintarParteHorizontal(ancho) {
    for (var I = 0; I <= ancho + 1; I++) {
        if (I === 0 || I === ancho + 1) {
            res += "+";
        }
        else {
            res += "-";
        }
    }
    res += "\n";
}
/* Funcion principal del programa para crear un rombo dependiendo si es par o impar*/
function main(numAncho) {
    var caract = 2;
    espacios = numAncho / 2 - 1;
    // Si es par
    if (numAncho % 2 === 0) {
        //Pintar parte alta
        //Pintar parte alta del marco
        pintarParteHorizontal(numAncho);
        while (caract <= numAncho) {
            pintarMarco(numAncho);
            pintarBlancos(espacios);
            pintarFila(caract, numAncho);
            pintarBlancos(espacios);
            pintarMarco(numAncho);
            caract += 2;
            res += "\n";
            espacios -= 1;
        }
        //Pintar parte baja
        var caractB = numAncho - 2;
        espacios = 1;
        while (caractB > 0) {
            pintarMarco(numAncho);
            pintarBlancos(espacios);
            pintarFila(caractB, numAncho);
            pintarBlancos(espacios);
            pintarMarco(numAncho);
            caractB -= 2;
            res += "\n";
            espacios += 1;
        }
        pintarParteHorizontal(numAncho);
    }
    else {
        var caract_1 = 1;
        espacios = Math.floor(numAncho / 2);
        //Pintar parte alta del marco
        pintarParteHorizontal(numAncho);
        //Pintar parte alta
        while (caract_1 <= numAncho) {
            pintarMarco(numAncho);
            pintarBlancos(espacios);
            pintarFila(caract_1, numAncho);
            pintarBlancos(espacios);
            pintarMarco(numAncho);
            caract_1 += 2;
            res += "\n";
            espacios -= 1;
        }
        //Pintar parte baja
        var caractB = numAncho - 2;
        espacios = 1;
        while (caractB > 0) {
            pintarMarco(numAncho);
            pintarBlancos(espacios);
            pintarFila(caractB, numAncho);
            pintarBlancos(espacios);
            pintarMarco(numAncho);
            caractB -= 2;
            res += "\n";
            espacios += 1;
        }
        pintarParteHorizontal(numAncho);
    }
}
console.log(res);
/*
   ##
  ####
 ######
########
 ######
  ####
   ##
*/
