var numAncho = 37;
var res = "";
var espacios = 0;
main(numAncho);
function pintarBlancos(espacios) {
    for (var I = 0; I < espacios; I++) {
        res += " ";
    }
}
function pintarFila(numCaracter, ancho) {
    var totalPintar = numCaracter;
    for (var I = 0; I < ancho + 1; I++) {
        if (totalPintar > 0) {
            res += "#";
            totalPintar -= 1;
        }
    }
}
function main(numAncho) {
    var caract = 2;
    espacios = numAncho / 2 - 1;
    // Si es par
    if (numAncho % 2 === 0) {
        //Pintar parte alta
        while (caract <= numAncho) {
            pintarBlancos(espacios);
            pintarFila(caract, numAncho);
            caract += 2;
            res += "\n";
            espacios -= 1;
        }
        //Pintar parte baja
        var caractB = numAncho - 2;
        espacios = 1;
        while (caractB > 0) {
            pintarBlancos(espacios);
            pintarFila(caractB, numAncho);
            caractB -= 2;
            res += "\n";
            espacios += 1;
        }
    }
    else {
        var caract_1 = 1;
        espacios = Math.floor(numAncho / 2);
        //Pintar parte alta
        while (caract_1 <= numAncho) {
            pintarBlancos(espacios);
            pintarFila(caract_1, numAncho);
            caract_1 += 2;
            res += "\n";
            espacios -= 1;
        }
        //Pintar parte baja
        var caractB = numAncho - 2;
        espacios = 1;
        while (caractB > 0) {
            pintarBlancos(espacios);
            pintarFila(caractB, numAncho);
            caractB -= 2;
            res += "\n";
            espacios += 1;
        }
    }
}
console.log(res);
/*
   ##
  ####
 ######
########


*/
