let numAncho: number = 37;
let res: string = "";
let espacios: number = 0;
main(numAncho);

function pintarBlancos(espacios) {
  for (let I = 0; I < espacios; I++) {
    res += " ";
  }
}
function pintarFila(numCaracter, ancho) {
  let totalPintar = numCaracter;
  for (let I = 0; I < ancho + 1; I++) {
    if (totalPintar > 0) {
      res += "#";
      totalPintar -= 1;
    }
  }
}
/* Funcion principal del programa para crear un rombo dependiendo si es par o impar*/
function main(numAncho) {
  let caract: number = 2;
  espacios = numAncho / 2 - 1;
  // Si es par
  if (numAncho % 2 === 0) {
    //Pintar parte alta
    while (caract <= numAncho) {
      pintarBlancos(espacios);
      pintarFila(caract, numAncho);
      caract += 2;
      res += "\n";
      espacios -= 1;
    }

    //Pintar parte baja
    let caractB: number = numAncho - 2;
    espacios = 1;
    while (caractB > 0) {
      pintarBlancos(espacios);
      pintarFila(caractB, numAncho);
      caractB -= 2;
      res += "\n";
      espacios += 1;
    }
  } else {
    let caract: number = 1;
    espacios = Math.floor(numAncho / 2);
    //Pintar parte alta
    while (caract <= numAncho) {
      pintarBlancos(espacios);
      pintarFila(caract, numAncho);
      caract += 2;
      res += "\n";
      espacios -= 1;
    }

    //Pintar parte baja
    let caractB: number = numAncho - 2;
    espacios = 1;
    while (caractB > 0) {
      pintarBlancos(espacios);
      pintarFila(caractB, numAncho);
      caractB -= 2;
      res += "\n";
      espacios += 1;
    }
  }
}

console.log(res);

/*
   ##  
  ####  
 ###### 
########
 ###### 
  #### 
   ##    
*/
