/*Programa para recibir un string y darle la vuelta
Ej abcd == dcba 

*/

let cadena: string = "abcdefghi";
let resultado1: string = "Inversa: ";
let i: number = cadena.length;
//let j: number = 0;

/* Mostrar la cadena en orden
while (j < cadena.length - 1 ) {
  resultado1 += cadena[j];
  j--;
}
*/
do {
  i--;
  resultado1 += cadena[i];
} while (!(i == 0));
/*
while (i >= 0) {
  resultado1 += cadena[i];
  i--;
}*/
console.log(resultado1);
