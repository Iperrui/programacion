/*Programa para recibir un string y darle la vuelta
Ej abcd == dcba

*/
var cadena = "abcdefghi";
var resultado1 = "Inversa: ";
var i = cadena.length;
//let j: number = 0;
/* Mostrar la cadena en orden
while (j < cadena.length - 1 ) {
  resultado1 += cadena[j];
  j--;
}
*/
do {
    i--;
    resultado1 += cadena[i];
} while (!(i == 0));
/*
while (i >= 0) {
  resultado1 += cadena[i];
  i--;
}*/
console.log(resultado1);
