let nCaracter: number = 24; // Numero de caracteres que ocupa el ancho del tablero
let nCuadrados: number = 4; // Numero de cuadrados del tablero
let tablero: String = "";

main(nCaracter, nCuadrados);

/*
    Ejemplo: si nCaracter es 12, en cada cuadrado debe exister 3 caracteres: 12/4 = 3

    Asumir que nCaracter es divisible por nCuadrados


    --------------
    |###   ###   |
    |###   ###   |
    |###   ###   |
    |   ###   ###|
    |   ###   ###|
    |   ###   ###|
    |###   ###   |
    |###   ###   |
    |###   ###   |
    |   ###   ###|
    |   ###   ###|
    |   ###   ###|
    --------------
*/
/* Funcion PintarBlanco que recibe por paramero caracter(numero de caracter que tiene que 
  ir en cada uno de los cuadrado del tablero */
function pintarBlanco(caracter) {
  for (let Z = 0; Z < caracter; Z++) {
    tablero += " ";
  }
  tablero += "│";
}

/* Funcion PintarCuadrado que recibe por paramero caracter(numero de caracter que tiene que 
  ir en cada uno de los cuadrado del tablero pintando # */
function pintarCuadrado(caracter) {
  for (let Z = 0; Z < caracter; Z++) {
    tablero += "#";
  }
  tablero += "│";
}

/*Funcion que pinta el borde izquierdo del tablero*/
function pintarBordeIzquierdo() {
  tablero += "│";
}

/*Funcion que pinta el borde superior del tablero, dependiendo si es par o impar el numero de caracteres
 */
function pintarBordeSuperior(nCuadrados, caracter) {
  tablero += "┌";
  let cont = caracter;
  for (let Y = 0; Y < nCuadrados; Y++) {
    /*codigos:
    822  -
    741 esquina derecha 
   
    ctrl + shift + u  suelta y ecribe el codigo
    */
    if (cont == Y) {
      tablero += "┬";
      cont += caracter + 1;
    } else {
      tablero += "─";
    }
  }
  tablero += "┐";
}

function pintarBordeInferior(nCuadrados, caracter) {
  tablero += "└";
  let cont = caracter;
  for (let Y = 0; Y < nCuadrados; Y++) {
    /*codigos:
    822  -
    741 esquina derecha 
   
    ctrl + shift + u  suelta y ecribe el codigo
    */

    if (cont == Y) {
      tablero += "┴";
      cont += caracter + 1;
    } else {
      tablero += "─";
    }
  }
  tablero += "┘";
}

/* Funcion que pinta filas , si la el numero de cuadrado es par */
function pintarFilaPar(fila, caracter, nCuadrados) {
  // SI ES PAR el resto de dividir la fila entre 2
  if (fila % 2 === 0) {
    for (let Y = 0; Y < caracter; Y++) {
      pintarBordeIzquierdo();
      for (let X = 0; X < nCuadrados / 2; X++) {
        //cada vez que se ejecuta pinta dos cuadrados. Por lo que tenmos que pintar nCuadrados /  2
        pintarCuadrado(caracter);
        pintarBlanco(caracter);
      }
      tablero += "\n";
    }

    if ((fila = caracter)) {
      tablero += "├";
    } else {
      //Pintar linea baja del marco de cada fila.n Cuadrados - 1 | a�adidos en medio
      tablero += "│";
    }
    let cont = caracter;
    for (let Y = 0; Y < nCaracter + nCuadrados - 1; Y++) {
      if (cont == Y) {
        tablero += "┼";
        cont += caracter + 1;
      } else {
        tablero += "─";
      }
    }
    tablero += "┤";
    tablero += "\n";

    //SI ES IMPAR el resto de dividirlo entre 2
  } else {
    for (let Y = 0; Y < caracter; Y++) {
      pintarBordeIzquierdo();
      for (let X = 0; X < nCuadrados / 2; X++) {
        //cada vez que se ejecuta pinta dos cuadrados. Por lo que tenmos que pintar nCuadrados /  2
        pintarBlanco(caracter);
        pintarCuadrado(caracter);
      }
      tablero += "\n";
    }
    //Al ser pinta par sabemos que siempre acaba con un fila impar
    if (fila < nCuadrados - 1) {
      //Pintar linea baja del marco de cada fila nCuadrados - 1 por los | a�adidos

      if ((fila = caracter)) {
        tablero += "├";
      } else {
        //Pintar linea baja del marco de cada fila.n Cuadrados - 1 | a�adidos en medio
        tablero += "│";
      }
      let cont = caracter;
      for (let Y = 0; Y < nCaracter + nCuadrados - 1; Y++) {
        if (cont == Y) {
          tablero += "┼";
          cont += caracter + 1;
        } else {
          tablero += "─";
        }
      }
      tablero += "┤";
      tablero += "\n";
    }
  }
}

/* Funcion que pinta filas , si la el numero de cuadrado es impar */
function pintarFilaImpar(fila, caracter, nCuadrados) {
  // SI ES PAR

  if (fila % 2 === 0) {
    for (let Y = 0; Y < caracter; Y++) {
      pintarBordeIzquierdo();
      for (let COLUM = 0; COLUM < nCuadrados; COLUM++) {
        if (COLUM % 2 === 0) {
          pintarBlanco(caracter);
        } else {
          pintarCuadrado(caracter);
        }
      }
      tablero += "\n";
    }
    //Pintar linea baja del marco de cada fila nCuadrados - 1 por los | a�adidos
    tablero += "|";
    for (let Y = 0; Y < nCaracter + nCuadrados - 1; Y++) {
      tablero += "-";
    }
    tablero += "|";
    tablero += "\n";
    //SI ES IMPAR
  } else {
    for (let Y = 0; Y < caracter; Y++) {
      pintarBordeIzquierdo();
      for (let COLUM = 0; COLUM < nCuadrados; COLUM++) {
        if (COLUM % 2 === 0) {
          pintarCuadrado(caracter);
        } else {
          pintarBlanco(caracter);
        }
      }
      tablero += "\n";
    }

    //Pintar linea baja del marco de cada fila nCuadrados - 1 por los | a�adidos
    tablero += "|";
    for (let Y = 0; Y < nCaracter + nCuadrados - 1; Y++) {
      tablero += "-";
    }
    tablero += "|";
    tablero += "\n";
  }
}

function main(nCaracter, nCuadrados) {
  let caracter: number = nCaracter / nCuadrados;

  if (nCuadrados % 2 === 0) {
    pintarBordeSuperior(nCaracter + nCuadrados - 1, caracter);
    tablero += "\n";
    /*bucle que llama a pintarfilaPar pasando la fila que se va a pintar,numcaracter,numcuadrados*/
    for (let I = 0; I < nCuadrados; I++) {
      pintarFilaPar(I, caracter, nCuadrados);
    }
  } else {
    pintarBordeSuperior(nCaracter + nCuadrados - 1, caracter);
    tablero += "\n";
    for (let I = 0; I < nCuadrados; I++) {
      pintarFilaImpar(I, caracter, nCuadrados);
    }
  }
  pintarBordeInferior(nCaracter + nCuadrados - 1, caracter);
  console.log(tablero);
}
