let alto: number = 24;
let resultado: string = "";

main(alto);

function pintarEspacios(espacios) {
  for (let I = 0; I < espacios; I++) {
    resultado += " ";
  }
}
function pintarSimbolo(simbolo) {
  for (let I = 0; I < simbolo; I++) {
    resultado += "#";
  }
}
function pintarMarco(alto, fila) {
  if (fila === 0 || fila === alto) {
    resultado += "+";
  } else {
    resultado += "|";
  }
}
function pintarParteHorizontal(alto) {
  for (let I = 0; I <= alto + 1; I++) {
    if (I === 0 || I === alto + 1) {
      resultado += "+";
    } else {
      resultado += "-";
    }
  }
  resultado += "\n";
}
function main(alto) {
  //PAR
  if (alto % 2 === 0) {
    let contador: number = 1;
    let numFila: number = Math.floor(alto / 2); // nuemero de filas que va a pintar
    let simbolo: number = 1;
    let espacios: number = Math.floor(alto / 2) - 1;
    //Pintar parte alta del marco
    pintarParteHorizontal(alto - 1);
    //Parte alta del rombo
    while (contador <= numFila) {
      pintarMarco(alto, contador);
      pintarEspacios(espacios);
      pintarSimbolo(simbolo);
      pintarEspacios(espacios);
      pintarMarco(alto, contador);
      contador++;
      simbolo += 2;
      espacios--;
      resultado += "\n";
    }

    //Parte baja del rombo

    let contadorB: number = numFila;
    simbolo = alto - 1;
    espacios = 0;
    while (contadorB > 0) {
      pintarMarco(alto, contador);
      pintarEspacios(espacios);
      pintarSimbolo(simbolo);
      pintarEspacios(espacios);
      pintarMarco(alto, contador);
      contadorB--;
      simbolo -= 2;
      espacios++;
      resultado += "\n";
    }
    //pintar horizontal
    pintarParteHorizontal(alto - 1);
    console.log(resultado);
  } else {
    let contador: number = 1;
    let numFila: number = Math.floor(alto / 2) + 1; // nuemero de filas que va a pintar
    let simbolo: number = 1;
    let espacios: number = Math.floor(alto / 2);

    //Pintar parte alta del marco
    pintarParteHorizontal(alto);
    //Parte alta del rombo
    while (contador <= numFila) {
      pintarMarco(alto, contador);
      pintarEspacios(espacios);
      pintarSimbolo(simbolo);
      pintarEspacios(espacios);
      pintarMarco(alto, contador);
      contador++;
      simbolo += 2;
      espacios--;
      resultado += "\n";
    }

    //Parte baja del rombo

    let contadorB: number = numFila - 1;

    simbolo = alto - 2;
    espacios = 1;
    while (contadorB > 0) {
      pintarMarco(alto, contador);
      pintarEspacios(espacios);
      pintarSimbolo(simbolo);
      pintarEspacios(espacios);
      pintarMarco(alto, contador);
      contadorB--;
      simbolo -= 2;
      espacios++;
      resultado += "\n";
    }
    //pintar horizontal
    pintarParteHorizontal(alto);
    console.log(resultado);
  }
}
