/*Funcion que recibe un caracter y mira si es vocal*/
function esVocal(caracter) {
    var res = false;
    var vocales = ["a", "á", "e", "é", "i", "í", "o", "ó", "u", "ú"];
    var I = 0;
    do {
        if (caracter === vocales[I]) {
            res = true;
            I = vocales.length;
        }
        else {
            I++;
        }
    } while (I < vocales.length);
    return res;
}
var texto = "hola me llamo iván";
var contador = 0;
var caracter = "";
var resultadoTexto = "";
do {
    caracter = texto[contador];
    if (esVocal(caracter)) {
        resultadoTexto += caracter.toUpperCase();
        contador++;
    }
    else {
        resultadoTexto += caracter;
        contador++;
    }
} while (contador < texto.length);
console.log(resultadoTexto);
