/*Funcion que recibe un caracter y mira si es vocal minuscula */

function esVocal(caracter: string): boolean {
  let res = false;
  let vocales: string[] = ["a", "á", "e", "é", "i", "í", "o", "ó", "u", "ú"];
  let I = 0;
  do {
    if (caracter === vocales[I]) {
      res = true;
      I = vocales.length;
    } else {
      I++;
    }
  } while (I < vocales.length);

  return res;
}

let texto: string = "hola me llamo iván";

let contador: number = 0;
let caracter: string = "";
let resultadoTexto: string = "";
do {
  caracter = texto[contador];
  if (esVocal(caracter)) {
    resultadoTexto += caracter.toUpperCase();
    contador++;
  } else {
    resultadoTexto += caracter;
    contador++;
  }
} while (contador < texto.length);

console.log(resultadoTexto);
