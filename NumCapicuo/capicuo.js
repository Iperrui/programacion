var n1 = 222;
var n2 = 0;
var n3 = 0;
/*
Funcion: Recibe un numero
Devuelve: ese numero a la inversa
He usado la operacion modulo, ya que al hacer cualquier numero modulo 10 nos devuelve como resto
el ultimo numero. Y al dividir entre 10 y cogiendo la parte entera del numero, vamos recorriendo dicho numero

*/
function invertirNum(num) {
    var resto = num;
    var unidad = 0;
    do {
        n2 = n3 + (resto % 10); // Variable donde guardo el resto de dividir entre 10 y sumandolo a unidad
        resto = Math.floor(resto / 10); // Coger la parte entera de la division
        n3 = n2 * 10;
    } while (resto > 0);
    return n2;
}
if (n1 == invertirNum(n1)) {
    console.log("Es capicuo");
}
else {
    console.log("No es capicuo");
}
