/* 

num=0
resultado="Primos: "
    repetir
        num=num+1
        si es_primo(num) {
            entonces imprime(num)
        }
        fin si
    hasta num==9
*/

/*
funcion: que coje el numero y comprueba si es primo
devuelve: true si es primo y false si no lo es.
*/
function esPrimo(num: number): boolean {
  if (num == 1) {
    return true;
  } else if (divisible(num)) {
    return false;
  } else {
    return true;
  }
}
/*
      fucion que: Recibe como parametro un numero
      Devuelve: verdadero si tiene mas de un divisor y falso si solo tiene 2 divisores
      */
function divisible(num: number): boolean {
  let cont: number = 1; // contador que llega hasta el numero para acabar el while y es por lo que divido
  let div: number = 0; // un contador que cuenta los divisores del numero

  while (cont <= num) {
    if (num % cont == 0) {
      cont++;
      div++;
    } else {
      cont++;
    }
  }
  if (div == 2) {
    return false;
  } else {
    return true;
  }
}

let numero: number = 1;
let Nprimos: number = 100; // Lo n primeros primos
let resultado: string = "Primos: ";
while (numero <= Nprimos) {
  if (esPrimo(numero)) {
    resultado = resultado + numero + " ";
    numero++;
  } else {
    numero++;
  }
}
console.log(resultado);
