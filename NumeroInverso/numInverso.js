var num = 50214;
/*
Funcion: Recibe un numero
Devuelve: ese numero a la inversa
He usado la operacion modulo, ya que al hacer cualquier numero modulo 10 nos devuelve como resto
el ultimo numero. Y al dividir entre 10 y cogiendo la parte entera del numero, vamos recorriendo dicho numero

*/
function invertir(num) {
    var res = 0;
    var resto = num;
    var unidad = 0;
    do {
        res = unidad + (resto % 10); // Variable donde guardo el resto de dividir entre 10 y sumandolo a unidad
        resto = Math.floor(resto / 10); // Coger la parte entera de la division
        unidad = res * 10;
    } while (resto > 0);
    return res;
}
console.log(invertir(num));
