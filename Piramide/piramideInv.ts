let tam1: number = 10;
let res1: string = "";
pintar(tam1);

/*Funcion que recibe un numero y este pinta los signos correspondiente */
function pintarSigno(num) {
  let resultado: string = "";
  for (let X = 0; X < num; X++) {
    resultado += "#";
  }
  return resultado;
}

/*Funcion que  pinta los espacios en blacos que recibe como parametro */
function pintarVacio(num) {
  let resultado: string = "";
  for (let Y = 0; Y < num; Y++) {
    resultado += " ";
  }
  return resultado;
}

function pintar(tam1) {
  let fila: number = 1;
  for (let I = tam1; I > 0; I--) {
    res1 = ""; //reniciar la variable para que no pinte mas
    res1 += pintarVacio(I - 1);
    res1 += pintarSigno(fila);
    fila++;
    console.log(res1);
  }
}
