let tam: number = 10;
let res1: string = "";
pintarIzqu(tam);
pintarDerecha(tam);
pintarIzquInv(tam);
pintarDerechaInver(tam);

function pintarDerecha(tam) {
  for (let I = 0; I < tam; I++) {
    res1 = "";
    for (let Y = 0; Y < I + 1; Y++) {
      res1 += "#";
    }
    console.log(res1);
  }
  return res1;
}
function pintarDerechaInver(tam) {
  for (let I = 0; I < tam; I++) {
    res1 = "";
    for (let Y = tam; Y > I; Y--) {
      res1 += "#";
    }
    console.log(res1);
  }
}

/*Funcion que recibe un numero y este pinta los signos correspondiente */
function pintarSigno(num) {
  let resultado: string = "";
  for (let X = 0; X < num; X++) {
    resultado += "#";
  }
  return resultado;
}

/*Funcion que  pinta los espacios en blacos que recibe como parametro */
function pintarVacio(num) {
  let resultado: string = "";
  for (let Y = 0; Y < num; Y++) {
    resultado += " ";
  }
  return resultado;
}

function pintarIzquInv(tam1) {
  let fila: number = 0;
  for (let I = tam1; I > 0; I--) {
    res1 = ""; //reniciar la variable para que no pinte mas
    res1 += pintarVacio(fila);
    res1 += pintarSigno(I);

    fila++;
    console.log(res1);
  }
}

function pintarIzqu(tam1) {
  let fila: number = 1;
  for (let I = tam1; I > 0; I--) {
    res1 = ""; //reniciar la variable para que no pinte mas
    res1 += pintarVacio(I - 1);
    res1 += pintarSigno(fila);

    fila++;
    console.log(res1);
  }
  return res1;
}
