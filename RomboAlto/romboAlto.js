var alto = 8;
var resultado = "";
main(alto);
function pintarEspacios(espacios) {
    for (var I = 0; I < espacios; I++) {
        resultado += " ";
    }
}
function pintarSimbolo(simbolo) {
    for (var I = 0; I < simbolo; I++) {
        resultado += "#";
    }
}
function main(alto) {
    //PAR
    if (alto % 2 === 0) {
        var contador = 1;
        var numFila = Math.floor(alto / 2); // nuemero de filas que va a pintar
        var simbolo = 1;
        var espacios = Math.floor(alto / 2);
        //Parte alta del rombo
        while (contador <= numFila) {
            pintarEspacios(espacios);
            pintarSimbolo(simbolo);
            contador++;
            simbolo += 2;
            espacios--;
            resultado += "\n";
        }
        //Parte baja del rombo
        var contadorB = numFila;
        simbolo = alto - 1;
        espacios = 1;
        while (contadorB > 0) {
            pintarEspacios(espacios);
            pintarSimbolo(simbolo);
            contadorB--;
            simbolo -= 2;
            espacios++;
            resultado += "\n";
        }
        console.log(resultado);
    }
    else {
        var contador = 1;
        var numFila = Math.floor(alto / 2) + 1; // nuemero de filas que va a pintar
        var simbolo = 1;
        var espacios = Math.floor(alto / 2);
        //Parte alta del rombo
        while (contador <= numFila) {
            pintarEspacios(espacios);
            pintarSimbolo(simbolo);
            contador++;
            simbolo += 2;
            espacios--;
            resultado += "\n";
        }
        //Parte baja del rombo
        var contadorB = numFila - 1;
        simbolo = alto - 2;
        espacios = 1;
        while (contadorB > 0) {
            pintarEspacios(espacios);
            pintarSimbolo(simbolo);
            contadorB--;
            simbolo -= 2;
            espacios++;
            resultado += "\n";
        }
        console.log(resultado);
    }
}
