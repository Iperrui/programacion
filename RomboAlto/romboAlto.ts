let alto: number = 8;
let resultado: string = "";

main(alto);

function pintarEspacios(espacios) {
  for (let I = 0; I < espacios; I++) {
    resultado += " ";
  }
}
function pintarSimbolo(simbolo) {
  for (let I = 0; I < simbolo; I++) {
    resultado += "#";
  }
}
function main(alto) {
  //PAR
  if (alto % 2 === 0) {
    let contador: number = 1;
    let numFila: number = Math.floor(alto / 2); // nuemero de filas que va a pintar
    let simbolo: number = 1;
    let espacios: number = Math.floor(alto / 2);

    //Parte alta del rombo
    while (contador <= numFila) {
      pintarEspacios(espacios);
      pintarSimbolo(simbolo);
      contador++;
      simbolo += 2;
      espacios--;
      resultado += "\n";
    }

    //Parte baja del rombo

    let contadorB: number = numFila;
    simbolo = alto - 1;
    espacios = 1;
    while (contadorB > 0) {
      pintarEspacios(espacios);
      pintarSimbolo(simbolo);
      contadorB--;
      simbolo -= 2;
      espacios++;
      resultado += "\n";
    }
    console.log(resultado);
  } else {
    let contador: number = 1;
    let numFila: number = Math.floor(alto / 2) + 1; // nuemero de filas que va a pintar
    let simbolo: number = 1;
    let espacios: number = Math.floor(alto / 2);
    //Parte alta del rombo
    while (contador <= numFila) {
      pintarEspacios(espacios);
      pintarSimbolo(simbolo);
      contador++;
      simbolo += 2;
      espacios--;
      resultado += "\n";
    }

    //Parte baja del rombo

    let contadorB: number = numFila - 1;
    simbolo = alto - 2;
    espacios = 1;
    while (contadorB > 0) {
      pintarEspacios(espacios);
      pintarSimbolo(simbolo);
      contadorB--;
      simbolo -= 2;
      espacios++;
      resultado += "\n";
    }
    console.log(resultado);
  }
}
